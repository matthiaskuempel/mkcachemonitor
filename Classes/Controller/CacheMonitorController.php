<?php
namespace MK\Mkcachemonitor\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Controller to monitor the cache health of the TYPO3 system and extension specific caches.
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 * @version 0.5.0
 */
class CacheMonitorController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \MK\Mkcachemonitor\Domain\Factory\CacheInformationFactory Factory to obtain cache information instances.
     */
    protected $cacheInformationFactory = null;

    /**
     * @param \MK\Mkcachemonitor\Domain\Factory\CacheInformationFactory $cacheInformationFactory
     */
    public function injectCacheInformationFactory(\MK\Mkcachemonitor\Domain\Factory\CacheInformationFactory $cacheInformationFactory)
    {
        $this->cacheInformationFactory = $cacheInformationFactory;
    }

    /**
     * Display an overview of the current caching health information.
     *
     * @return void
     */
    public function dashboardAction()
    {
        $cacheConfigurations = $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'];
        $caches = [];
        foreach ($cacheConfigurations as $identifier => $configuration) {
            $caches[$identifier] = $configuration; // options, groups, frontend, backend...
            $caches[$identifier]['health'] = $this->getCacheInformation($identifier)->getHealth();
        }

        $this->view->assign("caches", $caches);
    }

    /**
     * Display detailed health information for the cache matching the specified identifier.
     *
     * @param string $identifier Cache identifier.
     *
     * @return void
     */
    public function healthAction(string $identifier)
    {
        $this->view->assign("health", $this->getCacheInformation($identifier)->getHealth());
        $this->view->assign("identifier", $identifier);
    }

    /**
     * Display the entries of the cache matching the specified identifier and (optional) tag.
     *
     * @param string $identifier Cache identifier.
     * @param string $tag        (Optional) tag used to filter the entries.
     *
     * @return void
     */
    public function listAction(string $identifier, string $tag = '')
    {
        $this->view->assign("entries", $this->getCacheInformation($identifier)->getEntries($tag));
        $this->view->assign("identifier", $identifier);
        $this->view->assign("tag", $tag);
    }

    /**
     * Obtain a cache information instance for the specified identifier.
     *
     * @param string $identifier Cache identifier.
     *
     * @return \MK\Mkcachemonitor\Domain\Model\CacheInformation Cache information instance for the specified identifier.
     */
    protected function getCacheInformation(string $identifier) : \MK\Mkcachemonitor\Domain\Model\CacheInformation
    {
        $backend = 'n/a';

        if (isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$identifier])) {
            $backend = trim(
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$identifier]['backend']
            );
        }

        return $this->cacheInformationFactory->getInstance($backend, $identifier);
    }
}