<?php
namespace MK\Mkcachemonitor\Domain\Factory;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Factory providing cache information instances.
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 * @version 0.5.0
 */
class CacheInformationFactory
{
    /**
     * @var array Containing the current implementations.
     */
    protected $implementations = [];

    /**
     * Provide a cache information instance supporting the specified backend and identifier.
     *
     * @param string $backend    Class name of the cache backend to provide a cache information instance for.
     * @param string $identifier Cache identifier / name to provide a cache information instance for.
     *
     * @return \MK\Mkcachemonitor\Domain\Model\CacheInformation A cache information implementation supporting the
     *                                                          specified cache backend.
     */
    public function getInstance(string $backend, string $identifier) : \MK\Mkcachemonitor\Domain\Model\CacheInformation
    {
        if (!isset($this->implementations[$backend][$identifier])) {
            $this->implementations[$backend][$identifier] = $this->createImplementation($backend, $identifier);
        }

        return $this->implementations[$backend][$identifier];
    }

    /**
     * Create a new cache information implementation supporting the specified backend and identifier.
     *
     * @param string $backend    Class name of the cache backend to create a cache information instance for.
     * @param string $identifier Cache identifier / name to create a cache information instance for.
     *
     * @return \MK\Mkcachemonitor\Domain\Model\CacheInformation A cache information implementation supporting the
     *                                                          specified cache backend.
     */
    protected function createImplementation(string $backend, string $identifier) : \MK\Mkcachemonitor\Domain\Model\CacheInformation
    {
        $implementation = null;

        switch(trim($backend)) {
            case 'TYPO3\CMS\Core\Cache\Backend\FileBackend':
                // intended fall through
            case 'TYPO3\CMS\Core\Cache\Backend\SimpleFileBackend':
                $implementation = new \MK\Mkcachemonitor\Domain\Model\FileCacheInformation($identifier);

                break;

            case 'TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend':
                $implementation = new \MK\Mkcachemonitor\Domain\Model\DbCacheInformation($identifier);

                break;

            case 'TYPO3\CMS\Core\Cache\Backend\RedisBackend':
                $implementation = new \MK\Mkcachemonitor\Domain\Model\RedisCacheInformation($identifier);

                break;

            default:
                $implementation = new \MK\Mkcachemonitor\Domain\Model\NoCacheInformation();

                break;
        }

        return $implementation;
    }
}