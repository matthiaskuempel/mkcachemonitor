<?php
namespace MK\Mkcachemonitor\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Cache information for cases in which no real cache information could be obtained.
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 * @version 0.4.0
 */
class NoCacheInformation implements CacheInformation
{
    /**
     * {inheritDoc}
     */
    public function getHealth() : array
    {
        return [
            'summary' => [
                'n/a' => 'n/a'
            ],
            'data' => [
                'n/a' => 'n/a'
            ]
        ];
    }

    /**
     * {inheritDoc}
     */
    public function getEntries(string $tags = null) : array
    {
        return [
            'n/a' => [
                'tags' => 'n/a',
                'info' => 'n/a',
                'format' => 'n/a',
                'preview' => 'n/a'
            ]
        ];
    }
}