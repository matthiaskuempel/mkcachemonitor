<?php
namespace MK\Mkcachemonitor\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Base class for cache information instances.
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 * @version 0.5.0
 */
abstract class AbstractCacheInformation
{
    /**
     * @var \TYPO3\CMS\Core\Cache\Backend\BackendInterface The TYPO3 cache backend of the current implementation.
     */
    protected $backend = null;

    /**
     * @var \TYPO3\CMS\Core\Cache\CacheManager Cache manager used to obtain the cache instances.
     */
    protected $cacheManager = null;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface Used to obtain the configuration.
     */
    protected $configurationManager = null;

    /**
     * @var array Containing the health data of the current cache.
     */
    protected $health = null;

    /**
     * @var string Cache identifier of the current implementation.
     */
    protected $identifier = null;

    /**
     * @var \TYPO3\CMS\Extbase\Utility\LocalizationUtility Used to provide translated text.
     */
    protected $localizationUtility = null;

    /**
     * @var \TYPO3\CMS\Core\Log\Logger Class specific logger. Gets dynamically instantiated, use the method logger() to
     *                                 access the logger.
     */
    protected $logger = null;

    /**
     * @var array Containing the current settings.
     */
    protected $settings = [];

    /**
     * @param \TYPO3\CMS\Core\Cache\CacheManager $cacheManager
     */
    public function injectCacheManager(\TYPO3\CMS\Core\Cache\CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(
        \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
    ) {
        $this->configurationManager = $configurationManager;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Utility\LocalizationUtility $localizationUtility
     */
    public function injectLocalizationUtility(\TYPO3\CMS\Extbase\Utility\LocalizationUtility $localizationUtility)
    {
        $this->localizationUtility = $localizationUtility;
    }

    /**
     * @param \TYPO3\CMS\Core\Log\LogManager $logManager
     */
    public function injectLogManager(\TYPO3\CMS\Core\Log\LogManager $logManager)
    {
        $this->logger = $logManager->getLogger(__CLASS__);
    }

    /**
     * Use this method to access a cache.
     *
     * @param string $identifier Identifier of the cache to access.
     *
     * @return \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface The specified cache frontend.
     *
     * @throws \TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException
     */
    protected function cache(string $identifier) : \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface
    {
        if ($this->cacheManager == null) {
            $this->injectCacheManager(
                \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Cache\CacheManager::class)
            );
        }

        return $this->cacheManager->getCache($identifier);
    }

    /**
     * Use this method to access the configuration manager functionality.
     *
     * @return \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface Configuration manager used to access the
     *                                                                        configuration.
     */
    protected function configurationManager() : \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
    {
        if ($this->configurationManager == null) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
                \TYPO3\CMS\Extbase\Object\ObjectManager::class
            );
            $this->injectConfigurationManager(
                $objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class)
            );
        }

        return $this->configurationManager;
    }

    /**
     * Use this method to access the localization utility.
     *
     * @return \TYPO3\CMS\Extbase\Utility\LocalizationUtility Used to provide translated text.
     */
    protected function localizationUtility() : \TYPO3\CMS\Extbase\Utility\LocalizationUtility
    {
        if ($this->localizationUtility == null) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
                \TYPO3\CMS\Extbase\Object\ObjectManager::class
            );
            $this->injectLocalizationUtility(
                $objectManager->get(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::class)
            );
        }

        return $this->localizationUtility;
    }

    /**
     * Use this method to access the logger functionality.
     *
     * @return \TYPO3\CMS\Core\Log\Logger Class specific logger instance.
     */
    protected function logger() : \TYPO3\CMS\Core\Log\Logger
    {
        if ($this->logger == null) {
            $this->injectLogManager(
                \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Log\LogManager::class)
            );
        }

        return $this->logger;
    }

    /**
     * Construct a new cache information instance for the specified cache identifier.
     *
     * @param string $identifier Cache identifier of the current implementation.
     */
    public function __construct(string $identifier)
    {
        $this->identifier = $identifier;
        $this->settings = $this->configurationManager()->getConfiguration(
            \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'mkcachemonitor'
        );
    }

    /**
     * The TYPO3 cache backend of the current implementation.
     *
     * @return \TYPO3\CMS\Core\Cache\Backend\BackendInterface The TYPO3 cache backend of the current implementation or
     *                                                        <samp>null</samp> if no cache could be accessed for the
     *                                                        current identifier.
     */
    public function getCacheBackend() : \TYPO3\CMS\Core\Cache\Backend\BackendInterface
    {
        if ($this->backend == null) {
            try {
                $frontend = $this->cache($this->identifier);
                if ($frontend !== null) {
                    $this->backend = $frontend->getBackend();
                }
            } catch (\TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException $nsce) {
                $this->logger()->error("Cant access cache [" . $this->identifier . "].", ["message" => $nsce->getMessage()]);
            }
        }

        return $this->backend;
    }

    /**
     * {inheritDoc}
     */
    public function getHealth() : array
    {
        if ($this->health == null) {
            $this->health = $this->obtainHealth();
        }

        return $this->health;
    }

    /**
     * The formatted size in kb (including the suffix 'kb').
     *
     * @param int $byteSize The size in bytes as an int value.
     *
     * @return string The formatted size in kb (including the suffix 'kb').
     */
    protected function getFormattedKb(int $byteSize) : string
    {
        return number_format(round($byteSize / 1024, 2), 2) . ' kb';
    }

    /**
     * Obtain the health data of the current cache.
     *
     * @return array Containing the health data of the current cache.
     */
    abstract protected function obtainHealth() : array;
}