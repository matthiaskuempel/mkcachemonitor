<?php
namespace MK\Mkcachemonitor\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Cache information for file backends.
 *
 * @see \TYPO3\CMS\Core\Cache\Backend\FileBackend
 * @see \TYPO3\CMS\Core\Cache\Backend\SimpleFileBackend
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 * @version 0.9.0
 */
class FileCacheInformation extends AbstractCacheInformation implements CacheInformation
{
    /**
     * @var array Containing information about the cached files.
     */
    protected $fileInformation = null;

    /**
     * {inheritDoc}
     */
    public function getEntries(string $tags = null) : array {
        return $this->getFileInformation();
    }

    /**
     * {inheritDoc}
     */
    protected function obtainHealth() : array
    {
        $health = [
            'summary' => [
                'n/a' => 'n/a'
            ],
            'data' => [
                'n/a' => 'n/a'
            ]
        ];

        if ($this->getCacheBackend() !== null) {
            $health['summary'] = [
                'cache.entries' => $this->getFileCount()
            ];

            $freeSpace = intval(@disk_free_space($this->getDirectoryName(false)));
            $health['data'] = [
                'cache.entries.directory' => $this->getDirectoryName(),
                'cache.entries.size' => $this->getFormattedKb($this->getDirectorySize()),
                'cache.entries.free' => $freeSpace > 0 ? $this->getFormattedKb($freeSpace) : 'n/a',
            ];
        }

        return $health;
    }

    /**
     * Obtain the name of the directory used by the current cache backend.
     *
     * @param bool $relative Indicating if only the path relative to the (TYPO3) <samp>PATH_site</samp> should be
     *                       returned.
     *
     * @return string The (shortened) name of the directory used by the current cache backend.
     */
    protected function getDirectoryName(bool $relative = true) : string
    {
        $directoryName = trim($this->getCacheBackend()->getCacheDirectory());

        // remove TYPO3 PATH_site from absolute directory
        if ($relative && strpos($directoryName, PATH_site) !== false) {
            $directoryName = substr($directoryName, strpos($directoryName, PATH_site) + strlen(PATH_site));
        }

        return $directoryName;
    }

    /**
     * Obtain the number of files of the current cache backend.
     *
     * @return int The number of files of the current cache backend.
     */
    protected function getFileCount() : int
    {
        return count($this->getFileInformation());
    }

    /**
     * Obtain the total size of the cache entries in the cache directory.
     *
     * @return int Total size of the cache entries in the cache directory in bytes.
     */
    protected function getDirectorySize() : int
    {
        $totalSize = 0;

        $entries = $this->getFileInformation();
        foreach ($entries as $entry) {
            $totalSize += intval($entry['size']);
        }

        return $totalSize;
    }

    /**
     * Obtain information about all files in the current cache backend.
     *
     * The cache information will have the following format:<br />
     * <code>
     * [
     *  <filename> => [
     *   'tags' => 'n/a', // not available for files
     *   'info' => <human readable file meta information, like size and creation etc.>,
     *   'format' => <the format of the file>,
     *   'preview' => <a preview of the contents of the file>,
     *   'size' => <The size of the entry in bytes>
     *  ],
     *  ...
     * ]
     * </code>
     *
     * @return array Containing information about all files in the current cache backend (see above).
     */
    protected function getFileInformation() : array
    {
        if ($this->fileInformation == null) {
            $fileInformation = [];

            $dir = $this->getCacheBackend()->getCacheDirectory();
            $files = @scandir($dir);
            if (is_array($files)) {
                foreach ($files as $file) {
                    if ($file == "." || $file == ".." || !is_file($dir . $file)) {
                        continue;
                    }

                    $accessed = date('Y-m-d h:i:s', fileatime($dir . $file));
                    $created = date('Y-m-d h:i:s', filectime($dir . $file));
                    $modified = date('Y-m-d h:i:s', filemtime($dir . $file));
                    $byteSize = filesize($dir . $file);
                    $size = $this->getFormattedKb($byteSize);
                    $info = $this->localizationUtility()->translate('cache.entry.size', 'mkcachemonitor') . ': ' . $size . '<br /><br />' .
                        $this->localizationUtility()->translate('cache.entry.created', 'mkcachemonitor') . ': ' . $created . '<br />' .
                        $this->localizationUtility()->translate('cache.entry.modified', 'mkcachemonitor') . ': ' . $modified . '<br />' .
                        $this->localizationUtility()->translate('cache.entry.accessed', 'mkcachemonitor') . ': ' . $accessed;

                    $fileInformation[$file] = [
                        'tags' => 'n/a',
                        'info' => $info,
                        'format' => 'n/a',
                        'preview' => 'n/a',
                        'size' => $byteSize
                    ];
                }
            }

            $this->fileInformation = $fileInformation;
        }

        return $this->fileInformation;
    }
}