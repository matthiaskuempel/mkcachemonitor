<?php
namespace MK\Mkcachemonitor\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Cache information for DB backends.
 *
 * @see \TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 * @version 0.5.0
 */
class DbCacheInformation extends AbstractCacheInformation implements CacheInformation
{
    /**
     * {inheritDoc}
     */
    public function getEntries(string $tags = null) : array{
        $entries = [
            'n/a' => [
                'tags' => 'n/a',
                'info' => 'n/a',
                'format' => 'n/a',
                'preview' => 'n/a'
            ]
        ];

        return $entries;
    }

    /**
     * {inheritDoc}
     */
    protected function obtainHealth() : array
    {
        $health = [
            'summary' => [
                'n/a' => 'n/a'
            ],
            'data' => [
                'n/a' => 'n/a'
            ]
        ];

        if ($this->getCacheBackend() !== null) {
            $cacheTableInformation = $this->getCacheTableInformation();
            $tagsTableInformation = $this->getTagsTableInformation();

            $health['summary'] = [
                'cache.entries' => $cacheTableInformation['count'],
                'cache.tags' => $tagsTableInformation['count']
            ];

            $health['data'] = [
                'cache.entries.table' => $this->getCacheBackend()->getCacheTable(),
                'cache.entries.size' => $this->getFormattedKb($this->getSize($this->getCacheBackend()->getCacheTable())),
                'cache.tags.table' => $this->getCacheBackend()->getTagsTable(),
                'cache.tags.size' => $this->getFormattedKb($this->getSize($this->getCacheBackend()->getTagsTable())),
            ];
        }

        return $health;
    }


    /**
     * Obtain information about the current caching table (record count, expiration etc.).
     *
     * The returned array will have the following format:<br />
     * <code>
     * [
     *  'count' => <total number of records>,
     *  'earliestExpiration' => <earliest expiration date (human readable format)>,
     *  'latestExpiration' => <latest expiration date (human readable format)>,
     *  'size' => <total size of the database table>
     * ]
     * </code>
     *
     * @return array Containing the record information (see above).
     */
    protected function getCacheTableInformation() : array
    {
        $information = [
            'count' => '0',
            'earliestExpiration' => 'n/a',
            'latestExpiration' => 'n/a',
            'size' => '[TODO]'
        ];

        $result = $this->getDatabaseConnection()->exec_SELECTquery(
            'count(*) as count, min(expires) as minimumExpiration, max(expires) as maximumExpiration',
            $this->getCacheBackend()->getCacheTable(),
            '1=1'
        );
        if ($result) {
            $row = $this->getDatabaseConnection()->sql_fetch_assoc($result);

            $information['count'] = $row['count'];
            $information['earliestExpiration'] = date('Y-m-d h:i:s', $row['minimumExpiration']);
            $information['latestExpiration'] = date('Y-m-d h:i:s', $row['maximumExpiration']);

            $this->getDatabaseConnection()->sql_free_result($result);
        } else {
            $this->logger()->error(
                'Cant obtain cache table information [' . $this->getCacheBackend()->getCacheTable() . '].',
                [
                    'message' => $this->getDatabaseConnection()->sql_error()
                ]
            );
        }

        return $information;
    }

    /**
     * Obtain information about the current tagging table (record count, tag count, etc.).
     *
     * The returned array will have the following format:<br />
     * <code>
     * [
     *  'count' => <total number of records>,
     *  'tags' => [
     *   <name of the tag> => <number of records matching this tag>,
     *   ...
     *  ],
     *  'size' => <total size of the database table>
     * ]
     * </code>
     *
     * @return array Containing the record information (see above).
     */
    protected function getTagsTableInformation() : array
    {
        $information = [
            'count' => '0',
            'tags' => [],
            'size' => '[TODO]'
        ];

        $result = $this->getDatabaseConnection()->exec_SELECTquery(
            'count(*) as count, tag',
            $this->getCacheBackend()->getTagsTable(),
            '1=1',
            'tag'
        );
        if ($result) {
            while($row = $this->getDatabaseConnection()->sql_fetch_assoc($result)) {
                $information['count'] += $row['count'];

                if (isset($row['tag'])) {
                    $information['tags'][$row['tag']] = $row['count'];
                }
            }

            $this->getDatabaseConnection()->sql_free_result($result);
        } else {
            $this->logger()->error(
                'Cant obtain tag table information [' . $this->getCacheBackend()->getTagsTable() . '].',
                [
                    'message' => $this->getDatabaseConnection()->sql_error()
                ]
            );
        }

        return $information;
    }

    /**
     * Determine the size of the specified DB table (in bytes).
     *
     * @param string $tableName Name of the table to determine the size of.
     *
     * @return int The size of the specified DB table (in bytes).
     */
    protected function getSize(string $tableName) : int
    {
        $size = 0;
        $result = $this->getDatabaseConnection()->exec_SELECTquery(
            '(data_length + index_length) as size',
            'information_schema.TABLES',
            'table_schema = "' . TYPO3_db . '" AND table_name = "' . $tableName . '"'
        );
        if ($result) {
            if($row = $this->getDatabaseConnection()->sql_fetch_assoc($result)) {
                $size = $row['size'];
            }

            $this->getDatabaseConnection()->sql_free_result($result);
        } else {
            $this->logger()->error(
                'Cant obtain table size [' . $tableName . '].',
                [
                    'message' => $this->getDatabaseConnection()->sql_error()
                ]
            );
        }

        return $size;
    }

    /**
     * The current database connection.
     *
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection The current database connection.
     */
    protected function getDatabaseConnection() : \TYPO3\CMS\Core\Database\DatabaseConnection
    {
        if ($this->databaseConnection === null) {
            $this->databaseConnection = $GLOBALS['TYPO3_DB'];
        }
        return $this->databaseConnection;
    }
}