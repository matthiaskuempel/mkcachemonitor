<?php
namespace MK\Mkcachemonitor\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Cache information for DB backends.
 *
 * @see \TYPO3\CMS\Core\Cache\Backend\RedisBackend
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 * @version 0.7.0
 */
class RedisCacheInformation extends AbstractCacheInformation implements CacheInformation
{
    const SLOWLOG_ENTRIES = 32;
    const SLOWLOG_ENTRY_LENGTH = 128;

    /**
     * {inheritDoc}
     */
    public function getEntries(string $tags = null) : array
    {
        $entries = [
            'n/a' => [
                'tags' => 'n/a',
                'info' => 'n/a',
                'format' => 'n/a',
                'preview' => 'n/a'
            ]
        ];

        return $entries;
    }

    /**
     * {inheritDoc}
     */
    protected function obtainHealth() : array
    {
        $health = [
            'summary' => [
                'n/a' => 'n/a'
            ],
            'data' => [
                'n/a' => 'n/a'
            ]
        ];

        if ($this->getCacheBackend() !== null) {
            $options = $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$this->identifier]['options'];

            $redisData = $this->redisData($options['hostname'], $options['port'], $options['database']);
            if ($redisData !== null) {
                $health['summary'] = [
                    'cache.entries' => $redisData['dbSize'],
                    'cache.redis.slowLog.len' => $redisData['slowLog']['len']
                ];

                $health['data'] = [];
                foreach ($redisData['info'] as $label => $value) {
                    if ($this->iswhitelistedInfoOption($label)) {
                        $health['data']['INFO (' . $label . ')'] = $value;
                    }
                }
                $invalidCount = 1;
                foreach ($redisData['slowLog']['entries'] as $entry) {
                    if (!is_array($entry) || count($entry) < 4) {
                        $health['data']['SLOWLOG invalid #' . str_pad($invalidCount++, 3, '0', STR_PAD_LEFT)] = print_r($entry, true);
                    } else {
                        $count = str_pad($entry[0], 4, '0', STR_PAD_LEFT);
                        $time = date("Y-m-d h:m:i", $entry[1]);
                        $duration = str_pad($entry[2], 6, ' ', STR_PAD_LEFT);
                        $command = implode(',', $entry[3]);

                        $health['data']['SLOWLOG #' . $count] = substr(
                            '[' . $time . '] [' . $duration . 'ms] [' . $command . ']',
                            0,
                            self::SLOWLOG_ENTRY_LENGTH
                    );
                    }
                }
            }
        }

        return $health;
    }

    /**
     * Obtain the available redis information.
     *
     * The returned array will have the following entries:<br />
     * <samp>
     * [
     *  'dbSize' => <number of entries in the current redis database (output of 'dbSize' command)>,
     *  'slowLog' => [
     *   'len' => <the current number of slow log of entries (output of 'slowLog' command)>,
     *   'entries' => <the current slow log entries (output of 'slowLog' command)>,
     *  ],
     *  'info' => [
     *   <all information provided by the redis 'info' command>
     *  ]
     * ]
     * </samp>
     *
     * @param string $hostname The redis hostname.
     * @param int    $port     The redis port.
     * @param int    $database The redis database.
     *
     * @return array The available redis information, <samp>null</samp> if no information could be obtained.
     */
    protected function redisData(string $hostname, int $port, int $database) : array
    {
        $redisData = null;
        if (!extension_loaded('redis')) {
            $this->logger()->critical('PHP extension "redis" not loaded.');

            return $redisData;
        }

        $connected = false;
        $redis = new \Redis();
        try {
            $connected = $redis->connect($hostname, $port);

            if ($connected) {
                if ($database > 0) {
                    $success = $redis->select($database);
                    if (!$success) {
                        $this->logger()->error(
                            'Cant select Redis database.',
                            [
                                'hostname' => $hostname,
                                'port' => $port,
                                'database' => $database
                            ]
                        );
                    } else {
                        $redisData = [
                            'dbSize' => $redis->dbSize(),
                            'slowLog' => [
                                'len' => $redis->slowlog('len'),
                                'entries' => $redis->slowlog('get', self::SLOWLOG_ENTRIES)
                            ],
                            'info' => $redis->info('all')
                        ];
                    }
                }

                $redis->close();
            }
        } catch (\Exception $e) {
            $this->logger()->error(
                'Cant connect to Redis server using.',
                [
                    'hostname' => $hostname,
                    'port' => $port,
                    'error' => $e->getMessage()
                ]
            );
        }

        return $redisData;
    }

    /**
     * Check if the specified option is whitelisted for the INFO command.
     *
     * @param string $option The option name to check.
     *
     * @return bool Indicating if the specified option is whitelisted.
     */
    protected function iswhitelistedInfoOption(string $option) : bool
    {
        $whitelist = null;

        if (isset($this->settings['cache']['redis']['info']['whitelist'])
                && trim($this->settings['cache']['redis']['info']['whitelist']) != '') {
            $whitelist = explode(',', $this->settings['cache']['redis']['info']['whitelist']);
        }

        return $whitelist == null || in_array($option, $whitelist);
    }
}