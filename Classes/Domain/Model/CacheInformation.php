<?php
namespace MK\Mkcachemonitor\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Matthias Kümpel <matthias.kuempel@gmx.net>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Cache information provided by each cache.
 *
 * @package    MK
 * @subpackage mkcachemonitor
 *
 * @author Matthias Kümpel <matthias.kuempel@gmx.net>
 * @version 0.5.0
 */
interface CacheInformation
{
    /**
     * Obtain the current cache health status.
     *
     * The returned array should have the following format<br />:
     * <code>
     * [
     *  'summary' => [
     *    <health label> => <health value>,
     *    ...
     *  ],
     *  'data' => [
     *    <health label> => <health value>,
     *    ...
     *  ]
     * ]
     * </code>
     *
     * @return array Containing the current cache health.
     */
    public function getHealth() : array;

    /**
     * Obtain the current cache entries.
     *
     * The containing the returned entries will have the following format:<br />
     * <code>
     * [
     *  <identifier> => [
     *   'tags' => <comma separated list of tags for this entry>,
     *   'info' => <implementation specific additional info as a human readable string>,
     *   'format' => <format of the cache entry (e.g. "string")>,
     *   'preview' => <preview of the contents of the cache entry (only if the format is supported)>
     *  ]
     * ]
     * </code>
     *
     * <b>NOTE:</b> Additional implementation specific information may be provided in the array.
     *
     * @param string $tag Optional tag. If specified and supported by the underlying cache backend, only entries
     *                    matching this tag will be returned.
     *
     * @return array Containing the current cache entries in the format described above.
     */
    public function getEntries(string $tag = '') : array;
}