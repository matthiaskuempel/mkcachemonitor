<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (TYPO3_MODE === 'BE') {
    /**
     * register new backend module
     */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'MK.mkcachemonitor',
		'system',          // Make module a submodule of 'system'
		'cachemonitor',    // Submodule key
		'',                // Position
		array(
			'CacheMonitor' => 'dashboard,health,list'
		),
		array(
			'access' => 'admin',
			'icon' => 'EXT:mkcachemonitor/Resources/Public/Icons/mod_cachemonitor.png',
			'labels' => 'LLL:EXT:mkcachemonitor/Resources/Private/Language/locallang.xlf'
		)
	);

	/**
     * Configure logging
     */
    $GLOBALS['TYPO3_CONF_VARS']['LOG']['MK']['mkcachemonitor']['writerConfiguration'] = [
        \TYPO3\CMS\Core\Log\LogLevel::DEBUG => [
            'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => [
                'logFile' => 'fileadmin/logs/' . date('Y-m-d') . '_mkcachemonitor.txt'
            ]
        ]
    ];
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'mkcachemonitor',
    'Configuration/TypoScript',
    'TYPO3 cache monitor'
);