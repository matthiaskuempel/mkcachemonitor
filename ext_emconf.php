<?php

/**********************************************************************
 * Extension Manager/Repository config file for ext: "mkcachemonitor" *
 **********************************************************************/

$EM_CONF['mkcachemonitor'] = [
    'title' => 'TYPO3 cache monitor',
    'description' => 'Monitor the status and health of the configured TYPO3 caches.',
    'category' => 'plugin',
    'author' => 'Matthias Kümpel',
    'author_email' => 'matthias.kuempel@gmx.net',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '0.8.0',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-9.9.99',
            'php' => '7.0.0'
        ],
        'conflicts' => [],
        'suggests' => []
    ]
];
